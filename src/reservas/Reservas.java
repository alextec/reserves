/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author coji
 */
public class Reservas {

    String name;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String name = args.length>0?args[0]:"no_name";
        new Reservas(name);

    }

    public Reservas(String name) throws IOException {
        System.out.println("INICI...");
        String rutaLock = "lock.txt";
        File fitxerLock = new File(rutaLock);
        fitxerLock.delete();

        String ruta = "/home/alex/Escriptori/fullaReserves.txt";

        File fullaReserves = new File(ruta);
        fullaReserves.delete();
        fullaReserves.createNewFile();
        Random random = new Random();
        int comensals = 0;
        boolean freeTables = testFreeTables(ruta);
        boolean bloked;
        while (testFreeTables(ruta)) {
            try {
                
                /**
                 * 
                 * / Faig sleep per donar opcions els altres
                 * 
                 */ 
                Thread.sleep(random.nextInt(500));
                
                //
                comensals = random.nextInt(10) + 1;
                System.out.println(name + " >> Intento reservar per " + comensals);
                
                /**
                 * // Si hi ha el lock espero fins que es desbloqueixi
                 * 
                 */
                while (fitxerLock.exists()) {
                    try {
                        Thread.sleep(500);
                        System.out.println(name + " >> Bloquejat! esperant...");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Reservas.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                /**
                 * 
                 * // Ara bloquejo jo creant el fitxer
                 * 
                 */
                fitxerLock.createNewFile();
                System.out.println(name + " >> BLOQUEJO!!");
                
                /**
                // Preparo el fitxer de reserves
                 * 
                 */
                FileWriter fw = new FileWriter(fullaReserves, true);
                PrintWriter pw = new PrintWriter(fw);
                
                /**
                 * 
                 * //  Si la taula està disponible la reservo
                 * 
                 */
                if (this.tableAvailable(ruta, comensals)) {
                    pw.println("Taula " + comensals + " Reservada"+ " >> " + name);
                    System.out.println(name + ">> Taula " + comensals + " Reservada");
                    
                } else { // Si no deixo constància del bloqueig.
                    pw.println("BLOCKED " + comensals+ " >> " + name);
                    System.out.println(name + " >> BLOCKED " + comensals);
                    
                }
                pw.close();  //  Tanco
                fitxerLock.delete(); // IMPORTANT: Elimino el bloqueig!!!
            } catch (InterruptedException ex) {
                Logger.getLogger(Reservas.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private boolean testFreeTables(String ruta) throws FileNotFoundException, IOException {
        Boolean[] nums = new Boolean[10];
        int tableCounter = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(new File(ruta)))) {
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains("Reservada")) {
                    tableCounter++;
                    if (tableCounter == 10) {
                        return false;

                    }
                }
            }
        }

        return true;
    }

    private boolean tableAvailable(String ruta, int table) throws FileNotFoundException, IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(ruta)))) {
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains("Taula " + table + " Reservada")) {
                    return false;
                }
            }
        }
        return true;
    }
}
